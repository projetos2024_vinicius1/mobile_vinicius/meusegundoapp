import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function TelaInicial({ navigation }) {
    return (
        <View style={styles.container}>
            <Text>
                Tela Inicial
            </Text>
            <Button
                title="Ir para Segunda página"
                onPress={() => navigation.navigate('SegundaTela')}
            />

            <Button
                title="Voltar para home"
                onPress={() => navigation.goBack()}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu: {
        padding: 10,
        margin: 5,
        backgroundColor: "lightblue",
        borderRadius: 5,
    }
});

export default TelaInicial