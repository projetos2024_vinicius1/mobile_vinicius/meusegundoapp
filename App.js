import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';
import Layout from './src/layout/LayoutDeTelaEstrutura';
import LayoutHorizontal from './src/layout/LayoutHorizontal';
import LayoutGrade from './src/layout/LayoutGrade';
import Components from './src/components/Component';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import TelaInicial from './src/telaInicial';
import SegundaTela from './src/segundaTela';
import Menu from './src/components/Menu';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="TelaInicial" component={TelaInicial}/>
        <Stack.Screen name="SegundaTela" component={SegundaTela}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  menu:{
    padding: 10,
    margin: 5,
    backgroundColor: "lightblue",
    borderRadius: 5,
  }
});
